using ECSContainerAnimationTask.Animations;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// ������������� ������� ��������� ���� ��� �����������
    /// </summary>
    [BurstCompile]
    [CreateBefore(typeof(AnimatorSystem))]
    public partial struct PortAreaBuilderSystem : ISystem, ISystemStartStop
    {
        /// <summary>
        /// ����� ������� �������
        /// </summary>
        /// <param name="state">������ �� ��������� �������</param>
        [BurstCompile]
        public void OnStartRunning(ref SystemState state)
        {
            EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
            EntityCommandBuffer.ParallelWriter ecbParallel = ecb.AsParallelWriter();
            foreach (var spawner in SystemAPI.Query<PortAreaSpawner>())
            {
                SpawnPortArea(ref state, ecbParallel, spawner);
            }

            state.Dependency.Complete();
            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }

        /// <summary>
        /// ����� ��������� �������
        /// </summary>
        /// <param name="state">������ �� ��������� �������</param>
        public void OnStopRunning(ref SystemState state) { }

        /// <summary>
        /// ����� ������ ���� � �����������
        /// </summary>
        /// <param name="chunkIndex">������ �����</param>
        private void SpawnPortArea(ref SystemState state, EntityCommandBuffer.ParallelWriter ecbParallel, PortAreaSpawner spawner)
        {
            for (int x = 0; x < spawner.PortSize.x; x++)
            {
                for (int y = 0; y < spawner.PortSize.y; y++)
                {
                    SpawnPlatformArea(ref state, ecbParallel, spawner, new int2(x, y));
                }
            }
        }

        /// <summary>
        /// ����� ������ ���� ����������� � ������������
        /// </summary>
        /// <param name="chunkIndex">������ �����</param>
        /// <param name="platformPosition">������� ���������</param>
        private void SpawnPlatformArea(ref SystemState state, EntityCommandBuffer.ParallelWriter ecbParallel, PortAreaSpawner spawner, int2 platformPosition)
        {
            var platform = platformPosition.x * spawner.PortSize.y + platformPosition.y;

            new PlatformrBuilderJob
            {
                ParallelWriter = ecbParallel,
                Platform = platform
            }.ScheduleParallel();

            for (int x = 0; x < spawner.PlatformSize.x; x++)
            {
                for (int y = 0; y < spawner.PlatformSize.y; y++)
                {
                    for (int z = 0; z < spawner.PlatformSize.z; z++)
                    {
                        var containerPosition = new int3(x, y, z);
                        var worldPosition = CalculatePosition(spawner, platformPosition, containerPosition);
                        var info = GetInfo(platform, containerPosition);

                        new ConteinerBuilderJob
                        {
                            ParallelWriter = ecbParallel,
                            Spawner = spawner,
                            Info = info,
                            Position = worldPosition
                        }.ScheduleParallel();
                    }
                }
            }
        }

        /// <summary>
        /// ����� ��������� ���������� ����������
        /// </summary>
        /// <param name="platform">����� ���������</param>
        /// <param name="containerPosition">������� ���������� � ���������</param>
        /// <returns>���������� ����������</returns>
        private ContainerInfo GetInfo(int platform, int3 containerPosition)
            => new ContainerInfo
            {
                Guid = $"{platform}-{containerPosition.x}-{containerPosition.z}-{containerPosition.y}",
                Row = containerPosition.z + 1,
                Level = containerPosition.y + 1,
                Section = containerPosition.x + 1,
                Platform = platform
            };

        /// <summary>
        /// ����� ������� ������� ���������� � ����
        /// </summary>
        /// <param name="platformPosition">������� ���������</param>
        /// <param name="containerPosition">������� ���������� � ���������</param>
        /// <returns>������� ���������� � ����</returns>
        private float3 CalculatePosition(PortAreaSpawner spawner, int2 platformPosition, int3 containerPosition)
        {
            var platformSize = new float2(
                spawner.PlatformSize.x * (spawner.ContainerSpacing.x + spawner.ContainerSize.x),
                spawner.PlatformSize.z * (spawner.ContainerSpacing.z + spawner.ContainerSize.z));

            return new float3(
                platformPosition.x * (spawner.PlatformSpacing.x + platformSize.x) + containerPosition.x * (spawner.ContainerSpacing.x + spawner.ContainerSize.x),
                containerPosition.y * (spawner.ContainerSpacing.y + spawner.ContainerSize.y),
                platformPosition.y * (spawner.PlatformSpacing.y + platformSize.y) + containerPosition.z * (spawner.ContainerSpacing.z + spawner.ContainerSize.z));
        }

        /// <summary>
        /// ������� ��������� ��������
        /// </summary>
        [BurstCompile]
        partial struct PlatformrBuilderJob : IJobEntity
        {
            /// <summary>
            /// ������ �������� �������� ��� ������������� �������
            /// </summary>
            public EntityCommandBuffer.ParallelWriter ParallelWriter;

            /// <summary>
            /// ����� ���������
            /// </summary>
            public int Platform;

            /// <summary>
            /// ����� ���������� �������
            /// </summary>
            /// <param name="chunkIndex">������ �����</param>
            /// <param name="spawner">������ �� ��������� �������� �����������</param>
            /// <param name="animationSettings">������ �� ��������� ��������</param>
            private void Execute([ChunkIndexInQuery] int chunkIndex)
            {
                var entity = ParallelWriter.CreateEntity(chunkIndex);
                ParallelWriter.AddComponent(chunkIndex, entity, new PlatformInspectionInfo { Platform = Platform, UppedSection = -1 });
            }
        }

        /// <summary>
        /// ������� ��������� �����������
        /// </summary>
        [BurstCompile]
        partial struct ConteinerBuilderJob : IJobEntity
        {
            /// <summary>
            /// ������ �������� �������� ��� ������������� �������
            /// </summary>
            public EntityCommandBuffer.ParallelWriter ParallelWriter;

            /// <summary>
            /// ��������� �������� �������� �����������
            /// </summary>
            public PortAreaSpawner Spawner;

            /// <summary>
            /// ���������� ����������
            /// </summary>
            public ContainerInfo Info;

            /// <summary>
            /// ������� ���������� � ����
            /// </summary>
            public float3 Position;

            /// <summary>
            /// ��������� �������� ��������
            /// </summary>
            private AnimationSettings _animationSettings;

            /// <summary>
            /// ����� ���������� �������
            /// </summary>
            /// <param name="chunkIndex">������ �����</param>
            /// <param name="spawner">������ �� ��������� �������� �����������</param>
            /// <param name="animationSettings">������ �� ��������� ��������</param>
            private void Execute([ChunkIndexInQuery] int chunkIndex, AnimationSettings animationSettings)
            {
                _animationSettings = animationSettings;
                SpawnContainer(chunkIndex);
            }

            /// <summary>
            /// ����� ������ ���� ����������� � ������������
            /// </summary>
            /// <param name="chunkIndex">������ �����</param>
            /// <param name="info">���������� ����������</param>
            /// <param name="position">������� ���������� � ����</param>
            private void SpawnContainer(int chunkIndex)
            {
                var entity = ParallelWriter.Instantiate(chunkIndex, Spawner.Prefab);
                //ParallelWriter.SetName(chunkIndex, entity, $"Container [{info.Platform} : ({info.Section}, {info.Row}, {info.Level})]");
                ParallelWriter.AddComponent<ContainerTag>(chunkIndex, entity);
                ParallelWriter.SetComponent(chunkIndex, entity, LocalTransform.FromPosition(Position));
                AddContainerInfo(chunkIndex, entity, Info);
                AddAnimator(chunkIndex, entity, Position, Info.Level);
            }

            /// <summary>
            /// ����� ��������� ��������� ���������� ����������
            /// </summary>
            /// <param name="chunkIndex">������ �����</param>
            /// <param name="entity">�������� ����������</param>
            /// <param name="info">���������� ����������</param>
            private void AddContainerInfo(int chunkIndex, Entity entity, ContainerInfo info)
            {
                ParallelWriter.AddSharedComponent(chunkIndex, entity, new SharedPlatform { Value = info.Platform });
                ParallelWriter.AddSharedComponent(chunkIndex, entity, new SharedSection { Value = info.Section });
                ParallelWriter.AddComponent(chunkIndex, entity, info);
            }

            /// <summary>
            /// ����� ���������� ���������
            /// </summary>
            /// <param name="chunkIndex">������ �����</param>
            /// <param name="entity">�������� ����������</param>
            /// <param name="position">������� ����������</param>
            /// <param name="level">������� ����������</param>
            private void AddAnimator(int chunkIndex, Entity entity, float3 position, int level)
            {
                var upPosition = position.y + _animationSettings.Height * (level - 1);
                var animations = new UnsafeList<AnimationData>(2, Allocator.Persistent)
            {
                new AnimationData
                {
                    Name = nameof(ContainerAnimation.PutUp),
                    StartValue = position.y,
                    EndValue = upPosition,
                    Duration = _animationSettings.Duration,
                    Ease = _animationSettings.UpEase
                },
                new AnimationData
                {
                    Name = nameof(ContainerAnimation.PutDown),
                    StartValue = upPosition,
                    EndValue = position.y,
                    Duration = _animationSettings.Duration,
                    Ease = _animationSettings.DownEase
                }
            };

                ParallelWriter.AddComponent(chunkIndex, entity, new Animator
                {
                    Animations = animations
                });
                ParallelWriter.AddComponent<AnimatorEnableableTag>(chunkIndex, entity);
                ParallelWriter.SetComponentEnabled<AnimatorEnableableTag>(chunkIndex, entity, false);
            }
        }
    }
}
