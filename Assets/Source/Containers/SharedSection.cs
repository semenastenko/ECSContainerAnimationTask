﻿using Unity.Entities;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Общий компонент секции
    /// </summary>
    public struct SharedSection : ISharedComponentData
    {
        /// <summary>
        /// Номер секции
        /// </summary>
        public int Value;
    }
}
