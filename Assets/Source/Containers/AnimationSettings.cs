﻿using ECSContainerAnimationTask.Animations;
using Unity.Entities;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Компонент настройки анимаций контейнеров
    /// </summary>
    public struct AnimationSettings : IComponentData
    {
        /// <summary>
        /// Высота подъема контейнеров
        /// </summary>
        public float Height;

        /// <summary>
        /// Длительность анимации
        /// </summary>
        public float Duration;

        /// <summary>
        /// Функция интерполяции при поднятии
        /// </summary>
        public EaseType UpEase;

        /// <summary>
        /// Функция интерполяции при опускании
        /// </summary>
        public EaseType DownEase;
    }
}
