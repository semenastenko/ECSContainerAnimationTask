﻿using Unity.Entities;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Тег контейнера
    /// </summary>
    public struct ContainerTag : IComponentData { }
}
