﻿using Unity.Entities;
using Unity.Burst;
using UnityEngine;

namespace ECSContainerAnimationTask.Animations
{
    /// <summary>
    /// Неуправляемая система обновления аниматоров контейнеров
    /// </summary>
    [BurstCompile]
    //[UpdateAfter(typeof(AnimatorSystem))]
    public partial struct ContainerAnimatorUpdateSystem : ISystem
    {
        /// <summary>
        /// Метод обновления системы
        /// </summary>
        /// <param name="state">Ссылка на состояние системы</param>
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            new AnimatorJob().ScheduleParallel();
        }

        /// <summary>
        /// Задание аниматора
        /// </summary>
        [BurstCompile]
        partial struct AnimatorJob : IJobEntity
        {
            /// <summary>
            /// Метод выполнения задания
            /// </summary>
            /// <param name="chunkIndex">Индекс чанка</param>
            /// <param name="aspect">Аспект аниматора контейнера</param>
            /// <param name="tag">Тег включения аниматора</param>
            private void Execute([ChunkIndexInQuery] int chunkIndex, ContainerAnimatorAspect aspect, AnimatorEnableableTag tag)
            {
                aspect.Update();
            }
        }
    }
}
