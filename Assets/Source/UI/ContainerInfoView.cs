using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ECSContainerAnimationTask.UI
{
    /// <summary>
    /// ����� ����������� ���������� ����������
    /// </summary>
    public class ContainerInfoView : MonoBehaviour
    {
        /// <summary>
        /// ���������� ������� �������
        /// </summary>
        private const int CURSOR_SIZE = 32;

        /// <summary>
        /// ����� ������ ����������
        /// </summary>
        [SerializeField]
        private TMP_Text sectionText;

        /// <summary>
        /// ����� ���� ����������
        /// </summary>
        [SerializeField]
        private TMP_Text rowText;

        /// <summary>
        /// ����� ������ ����������
        /// </summary>
        [SerializeField]
        private TMP_Text levelText;

        /// <summary>
        /// ����� guid ����������
        /// </summary>
        [SerializeField]
        private TMP_Text guidText;

        /// <summary>
        /// �������� ������ ����������
        /// </summary>
        public string Section
        {
            get => sectionText.text;
            set => sectionText.text = value;
        }

        /// <summary>
        /// �������� ���� ����������
        /// </summary>
        public string Row
        {
            get => rowText.text;
            set => rowText.text = value;
        }

        /// <summary>
        /// �������� �������� ����������
        /// </summary>
        public string Level
        {
            get => levelText.text;
            set => levelText.text = value;
        }

        /// <summary>
        /// �������� guid ����������
        /// </summary>
        public string GUID
        {
            get => guidText.text;
            set => guidText.text = value;
        }

        /// <summary>
        /// �������� ��������� ����������� ����������
        /// </summary>
        public bool IsEnable
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        /// <summary>
        /// ����� ���������� ����������
        /// </summary>
        /// <param name="section">����� ����������</param>
        /// <param name="row">��� ����������</param>
        /// <param name="level">������� ����������</param>
        /// <param name="guid">guid ����������</param>
        public void SetValues(int section, int row, int level, string guid)
        {
            Section = section.ToString();
            Row = row.ToString();
            Level = level.ToString();
            GUID = guid.ToString();
        }

        /// <summary>
        /// ����� ���������� ������� ����������� ��������� �� ������
        /// </summary>
        /// <param name="position">������� �� ������</param>
        public void SetTargetPosition(Vector2 position)
        {
            var upscaleValues = GetUpscaleValues();
            var size = (transform as RectTransform).rect.size;
            size = new Vector2(size.x * upscaleValues.x, size.y * upscaleValues.y);

            var xPosition = position.x + size.x / 2 + CURSOR_SIZE * upscaleValues.x;
            if (xPosition + size.x / 2 > Screen.width)
                xPosition = position.x - size.x / 2;

            var yPosition = position.y - size.y / 2 - CURSOR_SIZE * upscaleValues.y;
            if (yPosition - size.y / 2 < 0)
                yPosition = position.y + size.y / 2;

            transform.position = new Vector3(xPosition, yPosition);
        }

        /// <summary>
        /// ����� ��������� ������������ ���������� ������
        /// </summary>
        /// <returns>������������ ���������� ������</returns>
        private Vector2 GetUpscaleValues()
            => new Vector2(Screen.width / 1920f, Screen.height / 1080f);
    }
}
