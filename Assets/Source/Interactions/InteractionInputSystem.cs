﻿using ECSContainerAnimationTask.Input;
using Unity.Entities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace ECSContainerAnimationTask.Interactions
{
    /// <summary>
    /// Управляемая система управления взаимодействием
    /// </summary>
    public partial class InteractionInputSystem : SystemBase, InteractionInput.IBaseActions
    {
        /// <summary>
        /// Управление взаимодействием
        /// </summary>
        private InteractionInput _input;

        /// <summary>
        /// Метод обработчик создания системы
        /// </summary>
        protected override void OnCreate()
        {
            _input = new InteractionInput();
            _input.Base.SetCallbacks(this);
            _input.Enable();

            EntityManager.CreateSingleton<MousePositionData>();
            EntityManager.CreateSingletonBuffer<StartInteractionInputElement>();
        }

        /// <summary>
        /// Метод обработчик обновления системы
        /// </summary>
        protected override void OnUpdate()
        {

        }

        /// <summary>
        /// Метод обработчик управления взаимодействием
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnInteract(InputAction.CallbackContext context)
        {
            var bufferData = SystemAPI.GetSingletonBuffer<StartInteractionInputElement>();
            var mousePositionData = SystemAPI.GetSingleton<MousePositionData>();
            if (context.started)
                bufferData.Add(new StartInteractionInputElement { MousePosition = mousePositionData.MousePosition });
        }

        /// <summary>
        /// Метод обработчик управления положением мыши
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnMousePosition(InputAction.CallbackContext context)
        {
            var data = SystemAPI.GetSingletonRW<MousePositionData>();
            data.ValueRW.MousePosition = context.ReadValue<Vector2>();
        }
    }
}
