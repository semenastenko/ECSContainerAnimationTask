﻿using Unity.Entities;
using Unity.Mathematics;

namespace ECSContainerAnimationTask.Interactions
{
    /// <summary>
    /// Буффер начала взаимодействий
    /// </summary>
    [InternalBufferCapacity(8)]
    public struct StartInteractionInputElement : IBufferElementData
    {
        /// <summary>
        /// Позиция мыши
        /// </summary>
        public float2 MousePosition;
    }

}
