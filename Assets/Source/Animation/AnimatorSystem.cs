﻿using ECSContainerAnimationTask.Containers;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace ECSContainerAnimationTask.Animations
{
    /// <summary>
    /// Неуправляемая система аниматоров
    /// </summary>
    [BurstCompile]
    public partial struct AnimatorSystem : ISystem
    {
        /// <summary>
        /// Метод обновления системы
        /// </summary>
        /// <param name="state">Ссылка на состояние системы</param>
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            //foreach (var aspect in SystemAPI.Query<AnimatorAspect>().WithAll<AnimatorEnableableTag>())
            //{
            //    if (aspect.IsPlaying)
            //        aspect.Animate(SystemAPI.Time.DeltaTime);
            //    else
            //        SystemAPI.SetComponentEnabled<AnimatorEnableableTag>(aspect.Entity, false);
            //}

            var parallelWriter = GetEntityCommandBuffer(ref state);

            new AnimatorJob
            {
                ParallelWriter = parallelWriter,
                DeltaTime = SystemAPI.Time.DeltaTime
            }.ScheduleParallel();
        }

        /// <summary>
        /// Метод получения буффера комманды сущности для параллельного запуска
        /// </summary>
        /// <param name="state">Ссылка на состояние системы</param>
        /// <returns>буффер комманды сущности для параллельного запуска</returns>
        private EntityCommandBuffer.ParallelWriter GetEntityCommandBuffer(ref SystemState state)
        {
            var ecbSingleton = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);
            return ecb.AsParallelWriter();
        }

        /// <summary>
        /// Метод уничтожения системы
        /// </summary>
        /// <param name="state">Ссылка на состояние системы</param>
        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
            foreach (var aspect in SystemAPI.Query<AnimatorAspect>())
            {
                aspect.Dispose();
            }
        }

        /// <summary>
        /// Задание аниматора
        /// </summary>
        [BurstCompile]
        partial struct AnimatorJob : IJobEntity
        {
            /// <summary>
            /// Буффер комманды сущности для параллельного запуска
            /// </summary>
            public EntityCommandBuffer.ParallelWriter ParallelWriter;

            /// <summary>
            /// Время между кадрами
            /// </summary>
            public float DeltaTime;

            /// <summary>
            /// Метод выполнения задания
            /// </summary>
            /// <param name="chunkIndex">Индекс чанка</param>
            /// <param name="aspect">Аспект аниматора</param>
            /// <param name="tag">Тег включения аниматора</param>
            private void Execute([ChunkIndexInQuery] int chunkIndex, AnimatorAspect aspect, AnimatorEnableableTag tag)
            {
                if (aspect.IsPlaying)
                    aspect.Animate(DeltaTime);
                else
                    ParallelWriter.SetComponentEnabled<AnimatorEnableableTag>(chunkIndex, aspect.Entity, false);
            }
        }
    }
}
