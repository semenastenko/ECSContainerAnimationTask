﻿using Unity.Entities;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Общий компонент платформы
    /// </summary>
    public struct SharedPlatform : ISharedComponentData
    {
        /// <summary>
        /// Номер платформы
        /// </summary>
        public int Value;
    }
}
