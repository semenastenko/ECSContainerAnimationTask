﻿using ECSContainerAnimationTask.Animations;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Авторинг настроек создания зоны с контейнерами
    /// </summary>
    public class PortAreaSpawnerAuthoring : MonoBehaviour
    {
        /// <summary>
        /// Префаб контейнера
        /// </summary>
        public GameObject Prefab;

        /// <summary>
        /// Размер зоны с платформами
        /// </summary>
        public Vector2Int PortSize;

        /// <summary>
        /// Расстояния между платформами
        /// </summary>
        public Vector2 PlatformSpacing;

        /// <summary>
        /// Размер Платформы
        /// </summary>
        public Vector3Int PlatformSize;

        /// <summary>
        /// Рассояния между контейнерами
        /// </summary>
        public Vector3 ContainerSpacing;

        /// <summary>
        /// Размер контейнера
        /// </summary>
        public Vector3 ContainerSize;

        /// <summary>
        /// Расстояние подъема контейнеров при анимации
        /// </summary>
        public float AnimationDistance;

        /// <summary>
        /// Длительность анимации
        /// </summary>
        public float AnimationDuration;

        /// <summary>
        /// Функция интерполяции подъема
        /// </summary>
        public EaseType UpEase;

        /// <summary>
        /// Функция интерполяции опускания
        /// </summary>
        public EaseType DownEase;
    }

    /// <summary>
    /// Пекарь настроек создания зоны с контейнерами
    /// </summary>
    public class PortAreaSpawnerBaker : Baker<PortAreaSpawnerAuthoring>
    {
        /// <summary>
        /// Метод запекания
        /// </summary>
        /// <param name="authoring">авторинг настроек создания зоны с контейнерами</param>
        public override void Bake(PortAreaSpawnerAuthoring authoring)
        {
            AddComponent(new PortAreaSpawner
            {
                Prefab = GetEntity(authoring.Prefab),
                PortSize = new int2(authoring.PortSize.x, authoring.PortSize.y),
                PlatformSpacing = new float2(authoring.PlatformSpacing.x, authoring.PlatformSpacing.y),
                PlatformSize = new int3(authoring.PlatformSize.x, authoring.PlatformSize.y, authoring.PlatformSize.z),
                ContainerSpacing = new float3(authoring.ContainerSpacing.x, authoring.ContainerSpacing.y, authoring.ContainerSpacing.z),
                ContainerSize = new float3(authoring.ContainerSize.x, authoring.ContainerSize.y, authoring.ContainerSize.z)
            });
            AddComponent(new AnimationSettings
            {
                Height = authoring.AnimationDistance,
                Duration = authoring.AnimationDuration,
                UpEase = authoring.UpEase,
                DownEase = authoring.DownEase
            });
        }
    }
}
