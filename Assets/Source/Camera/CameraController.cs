using ECSContainerAnimationTask.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace ECSContainerAnimationTask.Enviroments
{
    /// <summary>
    /// ����� ���������� �������
    /// </summary>
    public class CameraController : MonoBehaviour, CameraInput.IBaseActions
    {
        /// <summary>
        /// ���������� �������
        /// </summary>
        private CameraInput _input;

        /// <summary>
        /// ������� �������� ����������� ������
        /// </summary>
        [SerializeField] float _slowSpeed;

        /// <summary>
        /// ���������� ����������� ������
        /// </summary>
        [SerializeField] float _fastSpeed;

        /// <summary>
        /// ���������������� ��������
        /// </summary>
        [SerializeField] float _sensetivity;

        /// <summary>
        /// ���� ��������� ������
        /// </summary>
        private bool _isSpeedUp;

        /// <summary>
        /// ����������� �������� ������
        /// </summary>
        private Vector3 _moveDirection;

        /// <summary>
        /// ����� ������������� ��� MonoBehaviour
        /// </summary>
        private void Awake()
        {
            _input = new CameraInput();
            _input.Base.SetCallbacks(this);
            _input.Enable();
        }

        /// <summary>
        /// ����� ���������� ����� ��� MonoBehaviour
        /// </summary>
        private void Update()
        {
            if (_moveDirection.magnitude != 0)
            {
                float speed;
                if (_isSpeedUp)
                    speed = _fastSpeed;
                else
                    speed = _slowSpeed;
                transform.Translate(_moveDirection * speed * Time.deltaTime);
            }
        }

        /// <summary>
        /// ����� ���������� ���������� �������� ������
        /// </summary>
        /// <param name="context">�������� ����������</param>
        public void OnLook(InputAction.CallbackContext context)
        {
            var axis = context.ReadValue<Vector2>();
            var currentRotation = transform.eulerAngles;
            currentRotation.x -= axis.y * _sensetivity * Time.deltaTime;
            currentRotation.y += axis.x * _sensetivity * Time.deltaTime;
            transform.rotation = Quaternion.Euler(currentRotation.x, currentRotation.y, 0);
        }

        /// <summary>
        /// ����� ���������� ���������� ������/����� �������� ������
        /// </summary>
        /// <param name="context">�������� ����������</param>
        public void OnLookAround(InputAction.CallbackContext context)
        {
            if (context.started)
                Cursor.lockState = CursorLockMode.Locked;
            else if (context.canceled)
                Cursor.lockState = CursorLockMode.None;
        }

        /// <summary>
        /// ����� ���������� ���������� �����������
        /// </summary>
        /// <param name="context">�������� ����������</param>
        public void OnMove(InputAction.CallbackContext context)
        {
            if (context.performed || context.canceled)
            {
                _moveDirection = context.ReadValue<Vector3>();
            }
        }

        /// <summary>
        /// ����� ���������� ���������� ���������
        /// </summary>
        /// <param name="context">�������� ����������</param>
        public void OnSpeedUp(InputAction.CallbackContext context)
        {
            if (context.started)
                _isSpeedUp = true;
            else if (context.canceled)
                _isSpeedUp = false;
        }
    }
}
