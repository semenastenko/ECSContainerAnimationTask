﻿using System;
using Unity.Collections;
using Unity.Entities;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Компонент информации контейнера
    /// </summary>
    public struct ContainerInfo : IComponentData
    {
        /// <summary>
        /// GUID контейнера
        /// </summary>
        public FixedString64Bytes Guid;

        /// <summary>
        /// Секция контейнера
        /// </summary>
        public int Section;

        /// <summary>
        /// Ряд контейнера
        /// </summary>
        public int Row;

        /// <summary>
        /// Уровень контейнера
        /// </summary>
        public int Level;

        /// <summary>
        /// Платформа контейнера
        /// </summary>
        public int Platform;
    }
}
