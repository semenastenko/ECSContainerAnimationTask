﻿using Unity.Entities;
using Unity.Mathematics;

namespace ECSContainerAnimationTask.Interactions
{
    /// <summary>
    /// Компонент позиции мыши
    /// </summary>
    public struct MousePositionData : IComponentData
    {
        /// <summary>
        /// Позиция мыши
        /// </summary>
        public float2 MousePosition;
    }
}
