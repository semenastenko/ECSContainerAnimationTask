﻿using ECSContainerAnimationTask.Containers;
using System;
using Unity.Entities;
using UnityEngine;

namespace ECSContainerAnimationTask.UI
{
    /// <summary>
    /// Класс коннтроллера UI
    /// </summary>
    public class UIController : MonoBehaviour
    {
        /// <summary>
        /// Отображение информации контейнера
        /// </summary>
        [SerializeField]
        private ContainerInfoView _infoView;

        /// <summary>
        /// Метод инициализации для MonoBehaviourы
        /// </summary>
        private void Awake()
        {
            var world = World.DefaultGameObjectInjectionWorld;
            world.EntityManager.CreateSingleton(new ContainerInfoViewData { View = _infoView });
        }
    }
}
