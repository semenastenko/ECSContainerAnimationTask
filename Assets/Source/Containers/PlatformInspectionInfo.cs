﻿using Unity.Entities;
using Unity.Mathematics;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Компонент проверки состояния платформы
    /// </summary>
    public struct PlatformInspectionInfo : IComponentData
    {
        /// <summary>
        /// Номер платформы
        /// </summary>
        public int Platform;

        /// <summary>
        /// номер поднятой секции, если -1, то нет поднятых секций
        /// </summary>
        public int UppedSection;
    }
}
