﻿using UnityEngine;

namespace ECSContainerAnimationTask.Animations
{
    /// <summary>
    /// Перечисления функций
    /// </summary>
    public enum EaseType
    {
        Linear,
        QuadIn,
        QuadOut,
        QuadInOut,
        CubicIn,
        CubicOut,
        CubicInOut,
        SinIn,
        SinOut,
        SinInOut,
        ExpIn,
        ExpOut,
        ExpInOut,
        CircIn,
        CircOut,
        CircInOut,
        ElasticIn,
        ElasticOut,
        ElasticInOut,
        BackIn,
        BackOut,
        BackInOut,
        BounceIn,
        BounceOut,
        BounceInOut
    }

    /// <summary>
    /// Функции интерполяции
    /// </summary>
    public struct Ease
    {
        private const float BACK_VALUE_IN_OR_OUT = 1.70158f;

        private const float BACK_VALUE_IN_AND_OUT = 2.5949095f;

        /// <summary>
        /// Метод интерполяции с применением функции
        /// </summary>
        /// <param name="ease">функция</param>
        /// <param name="k">занчение</param>
        /// <returns>результат интерполяции</returns>
        public static float FromEnum(EaseType ease, float k)
        {
            if (ease == EaseType.QuadIn)
                return k * k;
            if (ease == EaseType.QuadOut)
                return k * (2f - k);
            if (ease == EaseType.QuadInOut)
                return (k *= 2f) < 1f ? 0.5f * k * k : -0.5f * ((k -= 1f) * (k - 2f) - 1f);
            if (ease == EaseType.CubicIn)
                return k * k * k;
            if (ease == EaseType.CubicOut)
                return 1f + (k -= 1f) * k * k;
            if (ease == EaseType.CubicInOut)
                return (k *= 2f) < 1f ? 0.5f * k * k * k : 0.5f * ((k -= 2f) * k * k + 2f);
            if (ease == EaseType.SinIn)
                return 1f - Mathf.Cos(k * Mathf.PI / 2f);
            if (ease == EaseType.SinOut)
                return Mathf.Sin(k * Mathf.PI / 2f);
            if (ease == EaseType.SinInOut)
                return 0.5f * (1f - Mathf.Cos(Mathf.PI * k));
            if (ease == EaseType.ExpIn)
                return k == 0f ? 0f : Mathf.Pow(1024f, k - 1f);
            if (ease == EaseType.ExpOut)
                return k == 1f ? 1f : 1f - Mathf.Pow(2f, -10f * k);
            if (ease == EaseType.ExpInOut)
                return k == 0 || k == 1 ? k : ((k *= 2f) < 1f ? 0.5f * Mathf.Pow(1024f, k - 1f) : 0.5f * (-Mathf.Pow(2f, -10f * (k - 1f)) + 2f));
            if (ease == EaseType.CircIn)
                return 1f - Mathf.Sqrt(1f - k * k);
            if (ease == EaseType.CircOut)
                return Mathf.Sqrt(1f - (k -= 1f) * k);
            if (ease == EaseType.CircInOut)
                return (k *= 2f) < 1f ? -0.5f * (Mathf.Sqrt(1f - k * k) - 1) : 0.5f * (Mathf.Sqrt(1f - (k -= 2f) * k) + 1f);
            if (ease == EaseType.ElasticIn)
                return k == 0 || k == 1 ? k : -Mathf.Pow(2f, 10f * (k -= 1f)) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f);
            if (ease == EaseType.ElasticOut)
                return k == 0 || k == 1 ? k : Mathf.Pow(2f, -10f * k) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f) + 1f;
            if (ease == EaseType.ElasticInOut)
                return (k *= 2f) < 1f ? -0.5f * Mathf.Pow(2f, 10f * (k -= 1f)) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f) : Mathf.Pow(2f, -10f * (k -= 1f)) * Mathf.Sin((k - 0.1f) * (2f * Mathf.PI) / 0.4f) * 0.5f + 1f;
            if (ease == EaseType.BackIn)
                return k * k * ((BACK_VALUE_IN_OR_OUT + 1f) * k - BACK_VALUE_IN_OR_OUT);
            if (ease == EaseType.BackOut)
                return (k -= 1f) * k * ((BACK_VALUE_IN_OR_OUT + 1f) * k + BACK_VALUE_IN_OR_OUT) + 1f;
            if (ease == EaseType.BackInOut)
                return (k *= 2f) < 1f ? 0.5f * (k * k * ((BACK_VALUE_IN_AND_OUT + 1f) * k - BACK_VALUE_IN_AND_OUT)) : 0.5f * ((k -= 2f) * k * ((BACK_VALUE_IN_AND_OUT + 1f) * k + BACK_VALUE_IN_AND_OUT) + 2f);
            if (ease == EaseType.BounceIn)
                return 1f - FromEnum(EaseType.BounceOut, 1f - k);
            if (ease == EaseType.BounceOut)
            {
                if (k < 1f / 2.75f)
                {
                    return 7.5625f * k * k;
                }
                else if (k < 2f / 2.75f)
                {
                    return 7.5625f * (k -= 1.5f / 2.75f) * k + 0.75f;
                }
                else if (k < 2.5f / 2.75f)
                {
                    return 7.5625f * (k -= 2.25f / 2.75f) * k + 0.9375f;
                }
                else
                {
                    return 7.5625f * (k -= 2.625f / 2.75f) * k + 0.984375f;
                }
            }
            if (ease == EaseType.BounceInOut)
                return k < 0.5f ? FromEnum(EaseType.BounceIn, k * 2f) * 0.5f : FromEnum(EaseType.BounceOut, k * 2f - 1f) * 0.5f + 0.5f; ;
            return k;
        }
    }
}
