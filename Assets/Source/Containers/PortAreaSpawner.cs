﻿using Unity.Entities;
using Unity.Mathematics;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Компонент настроек создания контейнеров
    /// </summary>
    public struct PortAreaSpawner : IComponentData
    {
        public Entity Prefab;

        public int2 PortSize;

        public float2 PlatformSpacing;

        public int3 PlatformSize;

        public float3 ContainerSpacing;

        public float3 ContainerSize;
    }
}
