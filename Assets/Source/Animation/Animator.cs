﻿using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;

namespace ECSContainerAnimationTask.Animations
{
    /// <summary>
    /// Тег включения аниматора
    /// </summary>
    public struct AnimatorEnableableTag : IComponentData, IEnableableComponent { }

    /// <summary>
    /// Компонент аниматора
    /// </summary>
    public struct Animator : IComponentData
    {
        /// <summary>
        /// Список анимаций
        /// </summary>
        public UnsafeList<AnimationData> Animations;

        /// <summary>
        /// Флаг проигрывания анимации
        /// </summary>
        public bool IsPlaying;

        /// <summary>
        /// Индекс текущей анимации
        /// </summary>
        public int CurrentAnimationIndex;

        /// <summary>
        /// Данные текущей анимации
        /// </summary>
        public AnimationData CurrentAnimation;
    }
}
