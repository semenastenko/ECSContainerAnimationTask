﻿using ECSContainerAnimationTask.Animations;
using ECSContainerAnimationTask.Containers;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;

namespace ECSContainerAnimationTask.Interactions
{
    /// <summary>
    /// Управляемая система взаимодействия
    /// </summary>
    [UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
    [UpdateAfter(typeof(PhysicsInitializeGroup))]
    [UpdateBefore(typeof(PhysicsSimulationGroup))]
    public partial class InteractionSystem : SystemBase
    {
        /// <summary>
        /// Метод обработчик обновления системы
        /// </summary>
        protected override void OnUpdate()
        {
            CalculateMouseRaycast();
            CalculateClickRaycast();
        }

        /// <summary>
        /// Метод расчета рейкаста положения мыши
        /// </summary>
        private void CalculateMouseRaycast()
        {
            var mousePositionData = SystemAPI.GetSingleton<MousePositionData>();
            var viewData = SystemAPI.ManagedAPI.GetSingleton<ContainerInfoViewData>();
            var ray = Camera.main.ScreenPointToRay(new Vector3(mousePositionData.MousePosition.x, mousePositionData.MousePosition.y));
            if (Raycast(ray.origin, ray.GetPoint(100), out var hit))
            {
                var info = EntityManager.GetComponentData<ContainerInfo>(hit.Entity);
                viewData.View.IsEnable = true;
                viewData.View.SetTargetPosition(mousePositionData.MousePosition);
                viewData.View.SetValues(info.Section, info.Row, info.Level, info.Guid.ToString());
            }
            else
            {
                viewData.View.IsEnable = false;
            }
        }

        /// <summary>
        /// Метод расчета рейкаста положения мыши при клике
        /// </summary>
        private void CalculateClickRaycast()
        {
            var buffer = SystemAPI.GetSingletonBuffer<StartInteractionInputElement>();
            foreach (var data in buffer)
            {
                var ray = Camera.main.ScreenPointToRay(new Vector3(data.MousePosition.x, data.MousePosition.y));
                if (Raycast(ray.origin, ray.GetPoint(100), out var hit))
                {
                    StartAnimation(hit.Entity);
                }
            }
            buffer.Clear();
        }

        /// <summary>
        /// Метод запуска анимации
        /// </summary>
        /// <param name="entity">Целевая сущность</param>
        private void StartAnimation(Entity entity)
        {
            var containerInfo = EntityManager.GetComponentData<ContainerInfo>(entity);

            foreach (var platformInfo in SystemAPI.Query<RefRW<PlatformInspectionInfo>>())
            {
                if (platformInfo.ValueRO.Platform == containerInfo.Platform)
                {
                    var platformInfoRO = platformInfo.ValueRO;
                    EntityQuery group = GetEntityQuery(typeof(ContainerInfo), typeof(SharedSection), typeof(SharedPlatform));

                    if (platformInfoRO.UppedSection >= 0)
                    {
                        group.SetSharedComponentFilter(new SharedSection { Value = platformInfoRO.UppedSection }, new SharedPlatform { Value = platformInfoRO.Platform });
                        StartPutDownAnimation(group.ToEntityArray(Allocator.Temp));
                        platformInfo.ValueRW.UppedSection = -1;
                    }

                    if (platformInfoRO.UppedSection < 0 || platformInfoRO.UppedSection != containerInfo.Section)
                    {
                        group.SetSharedComponentFilter(new SharedSection { Value = containerInfo.Section }, new SharedPlatform { Value = containerInfo.Platform });
                        StartPutUpAnimation(group.ToEntityArray(Allocator.Temp));
                        platformInfo.ValueRW.UppedSection = containerInfo.Section;
                    }
                }
            }
        }

        /// <summary>
        /// Метод запуска анимации опускания
        /// </summary>
        /// <param name="entities">Сущности для анимации</param>
        private void StartPutDownAnimation(NativeArray<Entity> entities)
        {
            foreach (var targetEntity in entities)
            {
                var animator = EntityManager.GetAspect<ContainerAnimatorAspect>(targetEntity);
                SystemAPI.SetComponentEnabled<AnimatorEnableableTag>(targetEntity, true);

                animator.PlayPutDownAnimation();
            }
        }

        /// <summary>
        /// Метод запуска анимации поднятия
        /// </summary>
        /// <param name="entities">Сущности для анимации</param>
        private void StartPutUpAnimation(NativeArray<Entity> entities)
        {
            foreach (var targetEntity in entities)
            {
                var animator = EntityManager.GetAspect<ContainerAnimatorAspect>(targetEntity);

                SystemAPI.SetComponentEnabled<AnimatorEnableableTag>(targetEntity, true);

                animator.PlayPutUpAnimation();
            }
        }

        /// <summary>
        /// Метод рейкаста
        /// </summary>
        /// <param name="RayFrom">Начало рейкаста</param>
        /// <param name="RayTo">Конец рейкаста</param>
        /// <param name="hit">Попадпние рейкаста</param>
        /// <returns>Если true - было зафиксировано поподание рейкста</returns>
        public bool Raycast(float3 RayFrom, float3 RayTo, out Unity.Physics.RaycastHit hit)
        {
            var physicsWorld = SystemAPI.GetSingleton<PhysicsWorldSingleton>();

            RaycastInput input = new RaycastInput()
            {
                Start = RayFrom,
                End = RayTo,
                Filter = new CollisionFilter()
                {
                    BelongsTo = ~0u,
                    CollidesWith = ~0u,
                    GroupIndex = 0
                }
            };
            return physicsWorld.CastRay(input, out hit);
        }
    }
}
