﻿using ECSContainerAnimationTask.UI;
using Unity.Entities;

namespace ECSContainerAnimationTask.Containers
{
    /// <summary>
    /// Управляемый компонент объекта отображения информации контейнера
    /// </summary>
    public class ContainerInfoViewData : IComponentData
    {
        /// <summary>
        /// Объект отображения информации контейнера
        /// </summary>
        public ContainerInfoView View;
    }
}
