﻿using ECSContainerAnimationTask.Containers;
using Unity.Entities;
using Unity.Transforms;

namespace ECSContainerAnimationTask.Animations
{
    /// <summary>
    /// Перечисление анимаций контейнера
    /// </summary>
    public enum ContainerAnimation
    {
        PutUp,
        PutDown
    }

    /// <summary>
    /// Аспект аниматора контейнера
    /// </summary>
    readonly partial struct ContainerAnimatorAspect : IAspect
    {
        /// <summary>
        /// Сущность с аниматором
        /// </summary>
        public readonly Entity Entity;

        /// <summary>
        /// Аспект аниматора
        /// </summary>
        readonly AnimatorAspect Animator;

        /// <summary>
        /// Аспект трансформа
        /// </summary>
        readonly TransformAspect Transform;

        /// <summary>
        /// Тег контейнера, необходим для определения аспекта
        /// </summary>
        readonly RefRO<ContainerTag> Container;

        /// <summary>
        /// Метод обновления анимации
        /// </summary>
        public void Update()
        {
            if (Animator.IsPlaying)
            {
                ref var animation = ref  Animator.RunningAnimation;
                if (animation.Name == nameof(ContainerAnimation.PutUp) || animation.Name == nameof(ContainerAnimation.PutDown))
                {
                    var position = Transform.LocalPosition;
                    position.y = animation.Value;
                    Transform.LocalPosition = position;
                }
            }
        }

        /// <summary>
        /// Метод проигрывания анимации поднятия
        /// </summary>
        public void PlayPutUpAnimation()
        {
            Animator.TryPlayAnimationByName(nameof(ContainerAnimation.PutUp));
        }

        /// <summary>
        /// Метод проигрывания анимации опускания
        /// </summary>
        public void PlayPutDownAnimation()
        {
            Animator.TryPlayAnimationByName(nameof(ContainerAnimation.PutDown));
        }
    }
}
