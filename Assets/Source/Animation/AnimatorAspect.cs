﻿using System;
using System.Linq;
using Unity.Entities;
using UnityEngine;

namespace ECSContainerAnimationTask.Animations
{
    /// <summary>
    /// Аспект аниматора
    /// </summary>
    readonly partial struct AnimatorAspect : IAspect, IDisposable
    {
        /// <summary>
        /// Сущность с аниматором
        /// </summary>
        public readonly Entity Entity;

        /// <summary>
        /// Компонент аниматора доступный для чтения и записи
        /// </summary>
        readonly RefRW<Animator> Animator;

        /// <summary>
        /// Свойство флага проигрывания анимации
        /// </summary>
        public bool IsPlaying
        {
            get => Animator.ValueRO.IsPlaying;
            set => Animator.ValueRW.IsPlaying = value;
        }

        /// <summary>
        /// Свойство количества анимаций в аниматоре
        /// </summary>
        public int AnimationCount => Animator.ValueRO.Animations.Length;

        /// <summary>
        /// Свойство индекса текущей анимации
        /// </summary>
        public int CurrentAnimationIndex
        {
            get => Animator.ValueRO.CurrentAnimationIndex;
            set
            {
                if (value >= -1 && value < AnimationCount)
                    Animator.ValueRW.CurrentAnimationIndex = value;
                else
                    throw new ArgumentOutOfRangeException(nameof(CurrentAnimationIndex));
            }
        }

        /// <summary>
        /// Свойство данных текущей анимации
        /// </summary>
        public AnimationData CurrentAnimation
        {
            get => Animator.ValueRO.CurrentAnimation;
            private set => Animator.ValueRW.CurrentAnimation = value;
        }

        /// <summary>
        /// Свойство ссылка на запущенную анимацию
        /// </summary>
        public ref AnimationData RunningAnimation
            => ref Animator.ValueRO.Animations.ElementAt(CurrentAnimationIndex);

        /// <summary>
        /// Метод сброса аниматора
        /// </summary>
        public void Reset()
        {
            CurrentAnimationIndex = -1;
            IsPlaying = false;
            for (int i = 0; i < AnimationCount; i++)
            {
                ref var animation = ref Animator.ValueRW.Animations.ElementAt(i);
                animation.ElapsedTime = 0;
                animation.Value = animation.StartValue;
            }
        }

        /// <summary>
        /// Метод проигрывания анимации по индексу
        /// </summary>
        /// <param name="index">индекс анимации</param>
        public void PlayAnimationByIndex(int index)
        {
            Reset();
            CurrentAnimationIndex = index;
            CurrentAnimation = Animator.ValueRO.Animations[CurrentAnimationIndex];
            IsPlaying = true;
        }

        /// <summary>
        /// Метод проигрывания анимации по названию
        /// </summary>
        /// <param name="name">название анимации</param>
        /// <returns>Если true - удалось найти и запустить анимацию</returns>
        public bool TryPlayAnimationByName(string name)
        {
            int index = -1;
            for (int i = 0; i < AnimationCount; i++)
            {
                var animation = Animator.ValueRO.Animations[i];
                if (animation.Name == name)
                {
                    index = i;
                    break;
                }
            }

            if (index >= 0)
            {
                PlayAnimationByIndex(index);
                return true;
            }
            return false;
        }


        /// <summary>
        /// Метод обновления анимации
        /// </summary>
        /// <param name="deltaTime">Время между кадрами</param>
        public void Animate(float deltaTime)
        {
            if (CurrentAnimationIndex != -1 && IsPlaying)
            {
                ref var animation = ref Animator.ValueRO.Animations.ElementAt(CurrentAnimationIndex);
                var finished = LerpNext(ref animation, deltaTime);
                if (finished)
                {
                    IsPlaying = false;
                    CurrentAnimationIndex = -1;
                }
            }
        }

        /// <summary>
        /// Метод интерполяции анимации
        /// </summary>
        /// <param name="animation">Ссылка на данные анимации</param>
        /// <param name="deltaTime">Время между кадрами</param>
        /// <returns>Если true - значение анимации достигло конечного результата</returns>
        private bool LerpNext(ref AnimationData animation, float deltaTime)
        {
            if (animation.ElapsedTime > animation.Duration)
                return true;
            float lerpValue = Mathf.LerpUnclamped(animation.StartValue, animation.EndValue, Ease.FromEnum(animation.Ease, animation.ElapsedTime / animation.Duration));
            animation.ElapsedTime += deltaTime;
            animation.Value = lerpValue;
            if (animation.ElapsedTime > animation.Duration)
                animation.Value = animation.EndValue;
            return false;

        }

        /// <summary>
        /// Метод утилизации
        /// </summary>
        public void Dispose()
        {
            Animator.ValueRW.Animations.Dispose();
        }
    }
}
