﻿using Unity.Collections;

namespace ECSContainerAnimationTask.Animations
{
    /// <summary>
    /// Данные анимации
    /// </summary>
    public struct AnimationData
    {
        /// <summary>
        /// Название анимации
        /// </summary>
        public FixedString64Bytes Name;

        /// <summary>
        /// Начальное значение анимации
        /// </summary>
        public float StartValue;

        /// <summary>
        /// Конечное значение анимации
        /// </summary>
        public float EndValue;

        /// <summary>
        /// Длительность анимации
        /// </summary>
        public float Duration;

        /// <summary>
        /// Интерполяция анимации
        /// </summary>
        public EaseType Ease;

        /// <summary>
        /// Теущее значение анимации
        /// </summary>
        public float Value;

        /// <summary>
        /// Пройденное время анимации
        /// </summary>
        public float ElapsedTime;
    }
}
